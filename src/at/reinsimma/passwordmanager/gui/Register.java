package at.reinsimma.passwordmanager.gui;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.SystemColor;
import javax.swing.SwingConstants;

import at.reinsimma.passwordmanager.main.CreateFile;
import at.reinsimma.passwordmanager.main.Main;

import java.awt.Label;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.JTextPane;

public class Register extends JFrame {
	
	public Register() {
		getContentPane().setBackground(new Color(26, 26, 26));
		initialize();
	}

	private void initialize() {
		this.setResizable(false);
		this.setBounds(100, 100, 1100, 700);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);

		JPanel pnl_content = new JPanel();
		pnl_content.setBackground(new Color(0, 0, 0, 0));
		pnl_content.setBounds(370, 50, 360, 550);
		getContentPane().add(pnl_content);
		SpringLayout sl_pnl_content = new SpringLayout();
		pnl_content.setLayout(sl_pnl_content);

			JLabel lbl_username = new JLabel("Benutzername *");
			sl_pnl_content.putConstraint(SpringLayout.WEST, lbl_username, 10, SpringLayout.WEST, pnl_content);
			sl_pnl_content.putConstraint(SpringLayout.EAST, lbl_username, -10, SpringLayout.EAST, pnl_content);
			lbl_username.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lbl_username.setForeground(Color.WHITE);
			pnl_content.add(lbl_username);

			JLabel lbl_heading = new JLabel("Registrieren");
			sl_pnl_content.putConstraint(SpringLayout.WEST, lbl_heading, 0, SpringLayout.WEST, lbl_username);
			sl_pnl_content.putConstraint(SpringLayout.EAST, lbl_heading, -10, SpringLayout.EAST, pnl_content);
			lbl_heading.setForeground(SystemColor.text);
			pnl_content.add(lbl_heading);
			lbl_heading.setHorizontalAlignment(SwingConstants.CENTER);
			lbl_heading.setFont(new Font("Calibri Light", Font.PLAIN, 32));
	
			JTextField tf_username = new JTextField();
			sl_pnl_content.putConstraint(SpringLayout.SOUTH, lbl_username, -6, SpringLayout.NORTH, tf_username);
			sl_pnl_content.putConstraint(SpringLayout.SOUTH, tf_username, 200, SpringLayout.NORTH, pnl_content);
			sl_pnl_content.putConstraint(SpringLayout.NORTH, tf_username, 168, SpringLayout.NORTH, pnl_content);
			sl_pnl_content.putConstraint(SpringLayout.WEST, tf_username, 10, SpringLayout.WEST, pnl_content);
			sl_pnl_content.putConstraint(SpringLayout.EAST, tf_username, -10, SpringLayout.EAST, pnl_content);
			Main.defaultTextBox(tf_username);
			pnl_content.add(tf_username);
	
			JLabel lbl_masterpassword = new JLabel("Masterpasswort *");
			sl_pnl_content.putConstraint(SpringLayout.NORTH, lbl_masterpassword, 13, SpringLayout.SOUTH, tf_username);
			sl_pnl_content.putConstraint(SpringLayout.WEST, lbl_masterpassword, 10, SpringLayout.WEST, pnl_content);
			sl_pnl_content.putConstraint(SpringLayout.SOUTH, lbl_masterpassword, 28, SpringLayout.SOUTH, tf_username);
			lbl_masterpassword.setFont(new Font("Tahoma", Font.PLAIN, 12));
			lbl_masterpassword.setForeground(Color.WHITE);
			pnl_content.add(lbl_masterpassword);
	
			JPasswordField pf_masterpassword1 = new JPasswordField();
			sl_pnl_content.putConstraint(SpringLayout.SOUTH, pf_masterpassword1, 266, SpringLayout.NORTH, pnl_content);
			sl_pnl_content.putConstraint(SpringLayout.NORTH, pf_masterpassword1, 234, SpringLayout.NORTH, pnl_content);
			sl_pnl_content.putConstraint(SpringLayout.WEST, pf_masterpassword1, 10, SpringLayout.WEST, pnl_content);
			sl_pnl_content.putConstraint(SpringLayout.EAST, pf_masterpassword1, -10, SpringLayout.EAST, pnl_content);
			pnl_content.add(pf_masterpassword1);
			Main.defaultPasswordField(pf_masterpassword1);
	
			JButton btn_register = new JButton("Registrieren");
			sl_pnl_content.putConstraint(SpringLayout.WEST, btn_register, 10, SpringLayout.WEST, pnl_content);
			sl_pnl_content.putConstraint(SpringLayout.EAST, btn_register, 0, SpringLayout.EAST, lbl_username);
			pnl_content.add(btn_register);
			Main.defaultButton(btn_register);
			
			JPasswordField pf_masterpassword2 = new JPasswordField();
			sl_pnl_content.putConstraint(SpringLayout.NORTH, btn_register, 34, SpringLayout.SOUTH, pf_masterpassword2);
			sl_pnl_content.putConstraint(SpringLayout.SOUTH, btn_register, 66, SpringLayout.SOUTH, pf_masterpassword2);
			sl_pnl_content.putConstraint(SpringLayout.WEST, pf_masterpassword2, 10, SpringLayout.WEST, pnl_content);
			sl_pnl_content.putConstraint(SpringLayout.EAST, pf_masterpassword2, -10, SpringLayout.EAST, pnl_content);
			pnl_content.add(pf_masterpassword2);
			Main.defaultPasswordField(pf_masterpassword2);
			
			JLabel lblMasterpasswortWiederholen = new JLabel("Masterpasswort wiederholen*");
			sl_pnl_content.putConstraint(SpringLayout.NORTH, pf_masterpassword2, 6, SpringLayout.SOUTH, lblMasterpasswortWiederholen);
			sl_pnl_content.putConstraint(SpringLayout.SOUTH, pf_masterpassword2, 38, SpringLayout.SOUTH, lblMasterpasswortWiederholen);
			sl_pnl_content.putConstraint(SpringLayout.NORTH, lblMasterpasswortWiederholen, 9, SpringLayout.SOUTH, pf_masterpassword1);
			sl_pnl_content.putConstraint(SpringLayout.SOUTH, lblMasterpasswortWiederholen, 24, SpringLayout.SOUTH, pf_masterpassword1);
			sl_pnl_content.putConstraint(SpringLayout.WEST, lblMasterpasswortWiederholen, 10, SpringLayout.WEST, pnl_content);
			lblMasterpasswortWiederholen.setForeground(Color.WHITE);
			lblMasterpasswortWiederholen.setFont(new Font("Tahoma", Font.PLAIN, 12));
			pnl_content.add(lblMasterpasswortWiederholen);
			
			JTextPane txtpnAlleEingegebenenInformationen = new JTextPane();
			sl_pnl_content.putConstraint(SpringLayout.SOUTH, lbl_heading, -6, SpringLayout.NORTH, txtpnAlleEingegebenenInformationen);
			txtpnAlleEingegebenenInformationen.setFont(new Font("Tahoma", Font.PLAIN, 11));
			sl_pnl_content.putConstraint(SpringLayout.NORTH, lbl_username, 8, SpringLayout.SOUTH, txtpnAlleEingegebenenInformationen);
			sl_pnl_content.putConstraint(SpringLayout.SOUTH, txtpnAlleEingegebenenInformationen, 139, SpringLayout.NORTH, pnl_content);
			sl_pnl_content.putConstraint(SpringLayout.NORTH, txtpnAlleEingegebenenInformationen, 56, SpringLayout.NORTH, pnl_content);
			sl_pnl_content.putConstraint(SpringLayout.WEST, txtpnAlleEingegebenenInformationen, 10, SpringLayout.WEST, pnl_content);
			sl_pnl_content.putConstraint(SpringLayout.EAST, txtpnAlleEingegebenenInformationen, -10, SpringLayout.EAST, pnl_content);
			txtpnAlleEingegebenenInformationen.setText("Alle eingegebenen Informationen k\u00F6nnen nicht mehr ge\u00E4ndert werden. Das Masterpasswort muss mindestens 12 Zeichen lang sein. Das Masterpasswort sollten Sie sonst nirgendwo verwenden. * = Pflichtfeld");
			txtpnAlleEingegebenenInformationen.setForeground(Color.WHITE);
			pnl_content.add(txtpnAlleEingegebenenInformationen);
			txtpnAlleEingegebenenInformationen.setBackground(new Color(26,26,26));
			
			JTextPane tp_error = new JTextPane();
			sl_pnl_content.putConstraint(SpringLayout.NORTH, tp_error, 15, SpringLayout.SOUTH, btn_register);
			sl_pnl_content.putConstraint(SpringLayout.WEST, tp_error, 0, SpringLayout.WEST, lbl_username);
			sl_pnl_content.putConstraint(SpringLayout.EAST, tp_error, 0, SpringLayout.EAST, lbl_username);
			tp_error.setForeground(Color.RED);
			pnl_content.add(tp_error);
			tp_error.setBackground(new Color(26,26,26));
			tp_error.setEditable(false);
			
			JButton btn_login = new JButton("Anmelden");
			btn_login.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent arg0) {
					
					Main.setApplicationWindow(new Login());

				}
				
			});
			sl_pnl_content.putConstraint(SpringLayout.NORTH, btn_login, -42, SpringLayout.SOUTH, pnl_content);
			sl_pnl_content.putConstraint(SpringLayout.WEST, btn_login, 0, SpringLayout.WEST, lbl_username);
			sl_pnl_content.putConstraint(SpringLayout.SOUTH, btn_login, -10, SpringLayout.SOUTH, pnl_content);
			sl_pnl_content.putConstraint(SpringLayout.EAST, btn_login, 0, SpringLayout.EAST, lbl_username);
			Main.defaultButton(btn_login);
			pnl_content.add(btn_login);
			
			JLabel lblSieBesitzenBereits = new JLabel("Sie besitzen bereits einen Accout? Hier anmelden!");
			sl_pnl_content.putConstraint(SpringLayout.SOUTH, tp_error, -14, SpringLayout.NORTH, lblSieBesitzenBereits);
			sl_pnl_content.putConstraint(SpringLayout.NORTH, lblSieBesitzenBereits, 512, SpringLayout.NORTH, pnl_content);
			sl_pnl_content.putConstraint(SpringLayout.WEST, lblSieBesitzenBereits, 0, SpringLayout.WEST, lbl_username);
			sl_pnl_content.putConstraint(SpringLayout.SOUTH, lblSieBesitzenBereits, -6, SpringLayout.NORTH, btn_login);
			lblSieBesitzenBereits.setForeground(Color.WHITE);
			lblSieBesitzenBereits.setFont(new Font("Tahoma", Font.PLAIN, 12));
			pnl_content.add(lblSieBesitzenBereits);
	
			Label lbl_copyrightInformation = new Label("\u00A9 2018-2019 Reinsimma GmbH");
			lbl_copyrightInformation.setBackground(new Color(26, 26, 26));
			lbl_copyrightInformation.setBounds(10, 639, 1074, 22);
			getContentPane().add(lbl_copyrightInformation);
			lbl_copyrightInformation.setAlignment(Label.CENTER);
			lbl_copyrightInformation.setForeground(Color.WHITE);
			btn_register.addActionListener(new ActionListener() {
	
				public void actionPerformed(ActionEvent arg0) {
	
					String tmp1 = tf_username.getText(), tmp2 = new String(pf_masterpassword1.getPassword()), tmp3 = new String(pf_masterpassword2.getPassword()), tmp = "";
	
					if(tmp1.equals("") || tmp2.equals("") || tmp3.equals("")) {
						tmp = "Es m�ssen alle Felder ausgef�llt werden. ";
					}
					
					if(tmp1.length() < Main.USERNAME_LENGTH) {
						tmp += ("Der Benutzername muss mindestens " + Main.USERNAME_LENGTH + " Zeichen lang sein. ");
					}
					
					if (tmp2.length() < Main.MASTERPASSWORD_LENGTH || tmp3.length() < Main.MASTERPASSWORD_LENGTH) {
						tmp += ("Das Masterpasswort muss mindestens " + Main.MASTERPASSWORD_LENGTH + " Zeichen lang sein. ");
					}
					if (tmp2.equals(tmp3) && tmp.equals("")) {
						Main.setUserName(tmp1);
						Main.setMasterPassword(tmp2);

						tmp = (CreateFile.createFile(tmp1));

					}
					
					else {
						tmp += ("Bitte geben Sie das Masterpasswort in beiden Felder korrekt ein. ");
							
					}
					tp_error.setText(tmp);
				}
	
			});

	}
}
