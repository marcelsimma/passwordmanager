package at.reinsimma.passwordmanager.gui;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;

import at.reinsimma.passwordmanager.encryption.Decrypt;
import at.reinsimma.passwordmanager.main.Main;

public class ListAccounts extends JFrame {

	public ListAccounts() {

		this.getContentPane().setBackground(Main.BACKGROUND);

		JLabel lbl_error = new JLabel("\u00A9 2018-2019 Reinsimma GmbH");
		lbl_error.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_error.setForeground(Color.WHITE);
		lbl_error.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbl_error.setBounds(0, 648, 1094, 23);
		getContentPane().add(lbl_error);

		this.setResizable(false);
		this.setBackground(Color.WHITE);
		this.setBounds(100, 100, 1100, 700);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);

		JButton btn_add = new JButton("Neu");
		btn_add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.setApplicationWindow(new DisplayAccountDetails(-1));
			}
		});
		btn_add.setBounds(972, 11, 112, 32);
		btn_add.setIcon(new ImageIcon("media/add.png"));
		Main.defaultButton(btn_add);
		getContentPane().add(btn_add);

		JLabel lblPasswordmanager = new JLabel("Accounts");
		lblPasswordmanager.setForeground(Color.WHITE);
		lblPasswordmanager.setFont(new Font("Calibri Light", Font.PLAIN, 32));
		lblPasswordmanager.setBounds(10, 11, 334, 40);
		this.getContentPane().add(lblPasswordmanager);

		JPanel panel = new JPanel();
		panel.setBackground(Main.BACKGROUND);
		panel.setBounds(1, 1, 0, 0);
		getContentPane().add(panel);

		JScrollPane scrollPane = new JScrollPane(panel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		scrollPane.setBorder(BorderFactory.createLineBorder(Color.WHITE, 0));
		scrollPane.setBounds(0, 63, 1094, 563);
		getContentPane().add(scrollPane);

		int i = 0;
		for (String[] c : Main.getAccounts()) {
			
			JButton button;

			try {
				button = new JButton(Decrypt.decrypt(c[0], Main.getMasterPassword()));
				button.setBounds(0, ++i * 52, 1100, 62);
				button.setHorizontalAlignment(SwingConstants.LEFT);
				button.setIcon(new ImageIcon("media/accountCircle.png"));
				button.setForeground(Color.white);
				button.setBackground(Main.BACKGROUND);
				button.setBorder(BorderFactory.createLineBorder(Color.WHITE, 0));

				button.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						int index = Main.getAccounts().indexOf(c);
						Main.setApplicationWindow(new DisplayAccountDetails(index));
					}

				});

				panel.add(button);

			} catch (InvalidKeyException | UnsupportedEncodingException | NoSuchAlgorithmException
					| NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException e) {
				System.out.println("Failed to decrypt account.");
				e.printStackTrace();
			}

		}

	}

}
