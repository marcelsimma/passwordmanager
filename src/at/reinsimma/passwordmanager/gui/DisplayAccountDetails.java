package at.reinsimma.passwordmanager.gui;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.JPasswordField;
import javax.swing.JPanel;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.BorderFactory;
import at.reinsimma.passwordmanager.encryption.Decrypt;
import at.reinsimma.passwordmanager.encryption.Encrypt;
import at.reinsimma.passwordmanager.main.Main;
import at.reinsimma.passwordmanager.main.WriteFile;

import javax.swing.ImageIcon;

public class DisplayAccountDetails extends JFrame {
	private static JTextField tf_username;
	private static JTextField tf_title;
	private static JPasswordField pf_password;
	private static JTextField tf_attribute1Name;
	private static JTextField tf_attribute2Name;
	private static JTextField tf_attribute3Name;
	private static JTextField tf_attribute4Name;
	private static JTextField tf_attribute5Name;
	private static JTextField tf_attribute6Name;
	private static JTextField tf_attribute7Name;
	private static JTextField tf_attribute8Name;
	private static JTextField tf_attribute9Name;
	private static JTextField tf_attribute10Name;
	private static JTextField tf_url;
	private static JTextField tf_attribute1Value;
	private static JTextField tf_attribute2Value;
	private static JTextField tf_attribute3Value;
	private static JTextField tf_attribute4Value;
	private static JTextField tf_attribute5Value;
	private static JTextField tf_attribute6Value;
	private static JTextField tf_attribute7Value;
	private static JTextField tf_attribute8Value;
	private static JTextField tf_attribute9Value;
	private static JTextField tf_attribute10Value;
	private JPanel panel;
	private JButton btn_delete;
	private JLabel lbl_title;
	private JButton btnPasswortGenerieren;
	private static JLabel lbl_error;
	private JButton btn_restore;
	private static int index;

	public DisplayAccountDetails(int i) {
		index = i;
		getContentPane().setBackground(Main.BACKGROUND);

		this.setResizable(false);
		this.setBackground(Main.BACKGROUND);
		this.setBounds(0, 0, 1100, 700);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		SpringLayout springLayout = new SpringLayout();
		getContentPane().setLayout(springLayout);

		tf_title = new JTextField();
		springLayout.putConstraint(SpringLayout.WEST, tf_title, 226, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, tf_title, -10, SpringLayout.EAST, getContentPane());
		tf_title.setBounds(10, 59, 1054, 44);
		getContentPane().add(tf_title);
		tf_title.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_title.setForeground(Color.WHITE);
		tf_title.setFont(new Font("Tahoma", Font.BOLD, 18));
		tf_title.setColumns(10);
		tf_title.setBackground(Main.BACKGROUND);

		JLabel lbl_username = new JLabel("Benutzername *");
		springLayout.putConstraint(SpringLayout.WEST, lbl_username, 10, SpringLayout.WEST, getContentPane());
		lbl_username.setBounds(20, 114, 132, 26);
		getContentPane().add(lbl_username);
		lbl_username.setForeground(Color.LIGHT_GRAY);
		lbl_username.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_username.setFont(new Font("Tahoma", Font.PLAIN, 12));

		tf_username = new JTextField();
		springLayout.putConstraint(SpringLayout.WEST, tf_username, 226, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lbl_username, -24, SpringLayout.WEST, tf_username);
		springLayout.putConstraint(SpringLayout.NORTH, tf_username, 20, SpringLayout.SOUTH, tf_title);
		springLayout.putConstraint(SpringLayout.SOUTH, tf_username, 52, SpringLayout.SOUTH, tf_title);
		tf_username.setBounds(181, 114, 861, 26);
		getContentPane().add(tf_username);
		tf_username.setForeground(Color.WHITE);
		tf_username.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tf_username.setColumns(10);
		tf_username.setBackground(Main.BACKGROUND);
		tf_username.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));

		JButton btn_clipboardUsername = new JButton(" Kopieren");
		springLayout.putConstraint(SpringLayout.EAST, tf_username, -6, SpringLayout.WEST, btn_clipboardUsername);
		springLayout.putConstraint(SpringLayout.WEST, btn_clipboardUsername, -138, SpringLayout.EAST, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, btn_clipboardUsername, 32, SpringLayout.NORTH, lbl_username);
		springLayout.putConstraint(SpringLayout.EAST, btn_clipboardUsername, -10, SpringLayout.EAST, getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, btn_clipboardUsername, 0, SpringLayout.NORTH, lbl_username);
		btn_clipboardUsername.setForeground(Color.WHITE);
		btn_clipboardUsername.setBounds(1048, 114, 26, 26);
		getContentPane().add(btn_clipboardUsername);
		btn_clipboardUsername.setBackground(Main.BUTTON_BACKGROUND);
		btn_clipboardUsername.setIcon(new ImageIcon("media/copy.png"));
		btn_clipboardUsername.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 0));
		btn_clipboardUsername.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				Main.copyToClipboard(tf_username.getText());

			}

		});

		JLabel lblPassword = new JLabel("Passwort *");
		springLayout.putConstraint(SpringLayout.SOUTH, lbl_username, -5, SpringLayout.NORTH, lblPassword);
		springLayout.putConstraint(SpringLayout.SOUTH, lblPassword, 185, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, lblPassword, 153, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblPassword, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblPassword, 202, SpringLayout.WEST, getContentPane());
		lblPassword.setBounds(20, 146, 132, 26);
		getContentPane().add(lblPassword);
		lblPassword.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPassword.setForeground(Color.LIGHT_GRAY);
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 12));

		pf_password = new JPasswordField();
		springLayout.putConstraint(SpringLayout.NORTH, pf_password, 154, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, pf_password, 24, SpringLayout.EAST, lblPassword);
		springLayout.putConstraint(SpringLayout.SOUTH, pf_password, 186, SpringLayout.NORTH, getContentPane());
		pf_password.setBounds(181, 146, 861, 26);
		getContentPane().add(pf_password);
		pf_password.setForeground(Color.WHITE);
		pf_password.setText("");
		pf_password.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		pf_password.setBackground(new Color(26, 26, 26));

		JButton btn_clipboardPassword = new JButton(" Kopieren");
		springLayout.putConstraint(SpringLayout.NORTH, btn_clipboardPassword, 0, SpringLayout.NORTH, lblPassword);
		springLayout.putConstraint(SpringLayout.SOUTH, btn_clipboardPassword, 32, SpringLayout.NORTH, lblPassword);
		springLayout.putConstraint(SpringLayout.EAST, btn_clipboardPassword, -10, SpringLayout.EAST, getContentPane());
		btn_clipboardPassword.setForeground(Color.WHITE);
		btn_clipboardPassword.setBackground(Main.BUTTON_BACKGROUND);
		btn_clipboardPassword.setBounds(1048, 146, 26, 26);
		getContentPane().add(btn_clipboardPassword);
		btn_clipboardPassword.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Main.copyToClipboard(new String(pf_password.getPassword()));

			}
		});
		btn_clipboardPassword.setBorder(BorderFactory.createLineBorder(Color.WHITE, 0));
		btn_clipboardPassword.setIcon(new ImageIcon("media/copy.png"));

		JLabel lblWebsite = new JLabel("Website");
		springLayout.putConstraint(SpringLayout.WEST, lblWebsite, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, lblWebsite, 191, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, lblWebsite, 223, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblWebsite, 202, SpringLayout.WEST, getContentPane());
		lblWebsite.setBounds(20, 178, 132, 26);
		getContentPane().add(lblWebsite);
		lblWebsite.setHorizontalAlignment(SwingConstants.RIGHT);
		lblWebsite.setForeground(Color.LIGHT_GRAY);
		lblWebsite.setFont(new Font("Tahoma", Font.PLAIN, 12));

		tf_url = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, tf_url, 191, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, tf_url, 24, SpringLayout.EAST, lblWebsite);
		springLayout.putConstraint(SpringLayout.SOUTH, tf_url, 223, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, tf_url, -144, SpringLayout.EAST, getContentPane());
		tf_url.setBounds(181, 178, 861, 26);
		getContentPane().add(tf_url);
		tf_url.setForeground(Color.WHITE);
		tf_url.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tf_url.setColumns(10);
		tf_url.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_url.setBackground(new Color(26, 26, 26));

		tf_attribute1Name = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute1Name, 229, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute1Name, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute1Name, 261, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute1Name, 202, SpringLayout.WEST, getContentPane());
		tf_attribute1Name.setBounds(20, 210, 132, 26);
		getContentPane().add(tf_attribute1Name);
		tf_attribute1Name.setHorizontalAlignment(SwingConstants.RIGHT);
		tf_attribute1Name.setForeground(Color.LIGHT_GRAY);
		tf_attribute1Name.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tf_attribute1Name.setColumns(10);
		tf_attribute1Name.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute1Name.setBackground(new Color(26, 26, 26));

		tf_attribute1Value = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute1Value, 5, SpringLayout.SOUTH, tf_url);
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute1Value, 24, SpringLayout.EAST, tf_attribute1Name);
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute1Value, 37, SpringLayout.SOUTH, tf_url);
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute1Value, -10, SpringLayout.EAST, getContentPane());
		tf_attribute1Value.setBounds(181, 210, 893, 26);
		getContentPane().add(tf_attribute1Value);
		tf_attribute1Value.setForeground(Color.WHITE);
		tf_attribute1Value.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tf_attribute1Value.setColumns(10);
		tf_attribute1Value.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute1Value.setBackground(new Color(26, 26, 26));

		tf_attribute2Name = new JTextField();
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute2Name, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute2Name, 267, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute2Name, 299, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute2Name, 202, SpringLayout.WEST, getContentPane());
		tf_attribute2Name.setBounds(20, 242, 132, 25);
		getContentPane().add(tf_attribute2Name);
		tf_attribute2Name.setHorizontalAlignment(SwingConstants.RIGHT);
		tf_attribute2Name.setForeground(Color.LIGHT_GRAY);
		tf_attribute2Name.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tf_attribute2Name.setColumns(10);
		tf_attribute2Name.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute2Name.setBackground(new Color(26, 26, 26));

		tf_attribute2Value = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute2Value, -1, SpringLayout.NORTH, tf_attribute2Name);
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute2Value, 24, SpringLayout.EAST, tf_attribute2Name);
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute2Value, 31, SpringLayout.NORTH, tf_attribute2Name);
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute2Value, -10, SpringLayout.EAST, getContentPane());
		tf_attribute2Value.setBounds(181, 242, 893, 26);
		getContentPane().add(tf_attribute2Value);
		tf_attribute2Value.setForeground(Color.WHITE);
		tf_attribute2Value.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tf_attribute2Value.setColumns(10);
		tf_attribute2Value.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute2Value.setBackground(new Color(26, 26, 26));

		tf_attribute3Name = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute3Name, 305, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute3Name, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute3Name, 337, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute3Name, 202, SpringLayout.WEST, getContentPane());
		tf_attribute3Name.setBounds(20, 273, 132, 26);
		getContentPane().add(tf_attribute3Name);
		tf_attribute3Name.setHorizontalAlignment(SwingConstants.RIGHT);
		tf_attribute3Name.setForeground(Color.LIGHT_GRAY);
		tf_attribute3Name.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tf_attribute3Name.setColumns(10);
		tf_attribute3Name.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute3Name.setBackground(new Color(26, 26, 26));

		tf_attribute3Value = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute3Value, -1, SpringLayout.NORTH, tf_attribute3Name);
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute3Value, 24, SpringLayout.EAST, tf_attribute3Name);
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute3Value, 31, SpringLayout.NORTH, tf_attribute3Name);
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute3Value, -10, SpringLayout.EAST, getContentPane());
		tf_attribute3Value.setBounds(181, 274, 893, 26);
		getContentPane().add(tf_attribute3Value);
		tf_attribute3Value.setForeground(Color.WHITE);
		tf_attribute3Value.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tf_attribute3Value.setColumns(10);
		tf_attribute3Value.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute3Value.setBackground(new Color(26, 26, 26));

		tf_attribute4Name = new JTextField();
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute4Name, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute4Name, 343, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute4Name, 375, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute4Name, 202, SpringLayout.WEST, getContentPane());
		tf_attribute4Name.setBounds(20, 305, 132, 26);
		getContentPane().add(tf_attribute4Name);
		tf_attribute4Name.setHorizontalAlignment(SwingConstants.RIGHT);
		tf_attribute4Name.setForeground(Color.LIGHT_GRAY);
		tf_attribute4Name.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tf_attribute4Name.setColumns(10);
		tf_attribute4Name.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute4Name.setBackground(new Color(26, 26, 26));

		tf_attribute4Value = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute4Value, 6, SpringLayout.SOUTH, tf_attribute3Value);
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute4Value, 24, SpringLayout.EAST, tf_attribute4Name);
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute4Value, 31, SpringLayout.NORTH, tf_attribute4Name);
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute4Value, -10, SpringLayout.EAST, getContentPane());
		tf_attribute4Value.setBounds(181, 306, 893, 25);
		getContentPane().add(tf_attribute4Value);
		tf_attribute4Value.setForeground(Color.WHITE);
		tf_attribute4Value.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tf_attribute4Value.setColumns(10);
		tf_attribute4Value.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute4Value.setBackground(new Color(26, 26, 26));

		tf_attribute5Name = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute5Name, 381, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute5Name, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute5Name, 413, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute5Name, 202, SpringLayout.WEST, getContentPane());
		tf_attribute5Name.setBounds(20, 337, 132, 26);
		getContentPane().add(tf_attribute5Name);
		tf_attribute5Name.setHorizontalAlignment(SwingConstants.RIGHT);
		tf_attribute5Name.setForeground(Color.LIGHT_GRAY);
		tf_attribute5Name.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tf_attribute5Name.setColumns(10);
		tf_attribute5Name.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute5Name.setBackground(new Color(26, 26, 26));

		tf_attribute5Value = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute5Value, -1, SpringLayout.NORTH, tf_attribute5Name);
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute5Value, 24, SpringLayout.EAST, tf_attribute5Name);
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute5Value, 31, SpringLayout.NORTH, tf_attribute5Name);
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute5Value, -10, SpringLayout.EAST, getContentPane());
		tf_attribute5Value.setBounds(181, 337, 893, 24);
		getContentPane().add(tf_attribute5Value);
		tf_attribute5Value.setForeground(Color.WHITE);
		tf_attribute5Value.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tf_attribute5Value.setColumns(10);
		tf_attribute5Value.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute5Value.setBackground(new Color(26, 26, 26));

		tf_attribute6Name = new JTextField();
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute6Name, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute6Name, 419, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute6Name, 451, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute6Name, 202, SpringLayout.WEST, getContentPane());
		tf_attribute6Name.setBounds(20, 369, 132, 26);
		getContentPane().add(tf_attribute6Name);
		tf_attribute6Name.setHorizontalAlignment(SwingConstants.RIGHT);
		tf_attribute6Name.setForeground(Color.LIGHT_GRAY);
		tf_attribute6Name.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tf_attribute6Name.setColumns(10);
		tf_attribute6Name.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute6Name.setBackground(new Color(26, 26, 26));

		tf_attribute6Value = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute6Value, 6, SpringLayout.SOUTH, tf_attribute5Value);
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute6Value, 24, SpringLayout.EAST, tf_attribute6Name);
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute6Value, 38, SpringLayout.SOUTH, tf_attribute5Value);
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute6Value, -10, SpringLayout.EAST, getContentPane());
		tf_attribute6Value.setBounds(181, 369, 893, 26);
		getContentPane().add(tf_attribute6Value);
		tf_attribute6Value.setForeground(Color.WHITE);
		tf_attribute6Value.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tf_attribute6Value.setColumns(10);
		tf_attribute6Value.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute6Value.setBackground(new Color(26, 26, 26));

		tf_attribute7Name = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute7Name, 457, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute7Name, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute7Name, 489, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute7Name, 202, SpringLayout.WEST, getContentPane());
		tf_attribute7Name.setBounds(20, 401, 132, 26);
		getContentPane().add(tf_attribute7Name);
		tf_attribute7Name.setHorizontalAlignment(SwingConstants.RIGHT);
		tf_attribute7Name.setForeground(Color.LIGHT_GRAY);
		tf_attribute7Name.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tf_attribute7Name.setColumns(10);
		tf_attribute7Name.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute7Name.setBackground(new Color(26, 26, 26));

		tf_attribute7Value = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute7Value, -1, SpringLayout.NORTH, tf_attribute7Name);
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute7Value, 24, SpringLayout.EAST, tf_attribute7Name);
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute7Value, 31, SpringLayout.NORTH, tf_attribute7Name);
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute7Value, -10, SpringLayout.EAST, getContentPane());
		tf_attribute7Value.setBounds(181, 401, 893, 26);
		getContentPane().add(tf_attribute7Value);
		tf_attribute7Value.setForeground(Color.WHITE);
		tf_attribute7Value.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tf_attribute7Value.setColumns(10);
		tf_attribute7Value.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute7Value.setBackground(new Color(26, 26, 26));

		tf_attribute8Name = new JTextField();
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute8Name, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute8Name, 495, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute8Name, 527, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute8Name, 202, SpringLayout.WEST, getContentPane());
		tf_attribute8Name.setBounds(20, 433, 132, 26);
		getContentPane().add(tf_attribute8Name);
		tf_attribute8Name.setHorizontalAlignment(SwingConstants.RIGHT);
		tf_attribute8Name.setForeground(Color.LIGHT_GRAY);
		tf_attribute8Name.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tf_attribute8Name.setColumns(10);
		tf_attribute8Name.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute8Name.setBackground(new Color(26, 26, 26));

		tf_attribute8Value = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute8Value, 6, SpringLayout.SOUTH, tf_attribute7Value);
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute8Value, 24, SpringLayout.EAST, tf_attribute8Name);
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute8Value, 38, SpringLayout.SOUTH, tf_attribute7Value);
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute8Value, -10, SpringLayout.EAST, getContentPane());
		tf_attribute8Value.setBounds(181, 433, 893, 26);
		getContentPane().add(tf_attribute8Value);
		tf_attribute8Value.setForeground(Color.WHITE);
		tf_attribute8Value.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tf_attribute8Value.setColumns(10);
		tf_attribute8Value.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute8Value.setBackground(new Color(26, 26, 26));

		tf_attribute9Name = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute9Name, 533, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute9Name, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute9Name, 565, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute9Name, 202, SpringLayout.WEST, getContentPane());
		tf_attribute9Name.setBounds(20, 465, 132, 26);
		getContentPane().add(tf_attribute9Name);
		tf_attribute9Name.setHorizontalAlignment(SwingConstants.RIGHT);
		tf_attribute9Name.setForeground(Color.LIGHT_GRAY);
		tf_attribute9Name.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tf_attribute9Name.setColumns(10);
		tf_attribute9Name.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute9Name.setBackground(new Color(26, 26, 26));

		tf_attribute9Value = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute9Value, -1, SpringLayout.NORTH, tf_attribute9Name);
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute9Value, 24, SpringLayout.EAST, tf_attribute9Name);
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute9Value, 31, SpringLayout.NORTH, tf_attribute9Name);
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute9Value, -10, SpringLayout.EAST, getContentPane());
		tf_attribute9Value.setBounds(181, 465, 893, 26);
		getContentPane().add(tf_attribute9Value);
		tf_attribute9Value.setForeground(Color.WHITE);
		tf_attribute9Value.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tf_attribute9Value.setColumns(10);
		tf_attribute9Value.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute9Value.setBackground(new Color(26, 26, 26));

		tf_attribute10Name = new JTextField();
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute10Name, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute10Name, 571, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute10Name, 603, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute10Name, 202, SpringLayout.WEST, getContentPane());
		tf_attribute10Name.setBounds(20, 497, 132, 26);
		getContentPane().add(tf_attribute10Name);
		tf_attribute10Name.setHorizontalAlignment(SwingConstants.RIGHT);
		tf_attribute10Name.setForeground(Color.LIGHT_GRAY);
		tf_attribute10Name.setFont(new Font("Tahoma", Font.PLAIN, 12));
		tf_attribute10Name.setColumns(10);
		tf_attribute10Name.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute10Name.setBackground(new Color(26, 26, 26));

		tf_attribute10Value = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, tf_attribute10Value, 6, SpringLayout.SOUTH, tf_attribute9Value);
		springLayout.putConstraint(SpringLayout.WEST, tf_attribute10Value, 24, SpringLayout.EAST, tf_attribute10Name);
		springLayout.putConstraint(SpringLayout.SOUTH, tf_attribute10Value, 38, SpringLayout.SOUTH, tf_attribute9Value);
		springLayout.putConstraint(SpringLayout.EAST, tf_attribute10Value, -10, SpringLayout.EAST, getContentPane());
		tf_attribute10Value.setBounds(181, 496, 893, 26);
		getContentPane().add(tf_attribute10Value);
		tf_attribute10Value.setForeground(Color.WHITE);
		tf_attribute10Value.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tf_attribute10Value.setColumns(10);
		tf_attribute10Value.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
		tf_attribute10Value.setBackground(new Color(26, 26, 26));

		lbl_title = new JLabel("Titel *");
		springLayout.putConstraint(SpringLayout.NORTH, lbl_title, 0, SpringLayout.NORTH, tf_title);
		springLayout.putConstraint(SpringLayout.WEST, lbl_title, 0, SpringLayout.WEST, lbl_username);
		springLayout.putConstraint(SpringLayout.SOUTH, lbl_title, 0, SpringLayout.SOUTH, tf_title);
		springLayout.putConstraint(SpringLayout.EAST, lbl_title, 0, SpringLayout.EAST, lbl_username);
		lbl_title.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl_title.setForeground(Color.LIGHT_GRAY);
		lbl_title.setFont(new Font("Tahoma", Font.PLAIN, 12));
		getContentPane().add(lbl_title);

		lbl_error = new JLabel("Alle nicht gespeicherten \u00C4nderungen gehen verloren.");
		springLayout.putConstraint(SpringLayout.NORTH, lbl_error, 37, SpringLayout.SOUTH, tf_attribute10Name);
		lbl_error.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbl_error.setForeground(Color.RED);
		springLayout.putConstraint(SpringLayout.WEST, lbl_error, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, lbl_error, -10, SpringLayout.SOUTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lbl_error, 0, SpringLayout.EAST, tf_title);
		getContentPane().add(lbl_error);

		btnPasswortGenerieren = new JButton(" Passwort generieren");
		springLayout.putConstraint(SpringLayout.WEST, btnPasswortGenerieren, 729, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btnPasswortGenerieren, -144, SpringLayout.EAST, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, pf_password, -6, SpringLayout.WEST, btnPasswortGenerieren);
		springLayout.putConstraint(SpringLayout.WEST, btn_clipboardPassword, 6, SpringLayout.EAST,
				btnPasswortGenerieren);
		btnPasswortGenerieren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (index >= 0) {
					int dialogButton = JOptionPane.YES_NO_OPTION;
					int dialogResult = JOptionPane.showConfirmDialog(null,
							"M�chten Sie wirklich ein neues Passwort generieren und dadurch das alte l�schen?", "Warnung", dialogButton);
					if (dialogResult == JOptionPane.YES_OPTION) {
						pf_password.setText(Main.generatePassword(12));
						lbl_error.setText("Passwort erfolgreich generiert.");
					}
				} else {
					pf_password.setText(Main.generatePassword(12));
					lbl_error.setText("Passwort erfolgreich generiert.");
				}
			}
		});
		springLayout.putConstraint(SpringLayout.NORTH, btnPasswortGenerieren, 0, SpringLayout.NORTH, lblPassword);
		springLayout.putConstraint(SpringLayout.SOUTH, btnPasswortGenerieren, 32, SpringLayout.NORTH, lblPassword);
		btnPasswortGenerieren.setForeground(Color.WHITE);
		btnPasswortGenerieren.setBorder(BorderFactory.createLineBorder(Color.WHITE, 0));
		btnPasswortGenerieren.setBackground(new Color(76, 76, 76));
		btnPasswortGenerieren.setIcon(new ImageIcon("media/create.png"));
		getContentPane().add(btnPasswortGenerieren);

		if (index >= 0) {
			setTextBoxes(index);
		}

		panel = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, tf_title, 16, SpringLayout.SOUTH, panel);
		springLayout.putConstraint(SpringLayout.SOUTH, tf_title, 48, SpringLayout.SOUTH, panel);
		springLayout.putConstraint(SpringLayout.NORTH, lbl_username, 68, SpringLayout.SOUTH, panel);
		panel.setBackground(Main.BACKGROUND);
		springLayout.putConstraint(SpringLayout.NORTH, panel, 0, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, panel, 0, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panel, 48, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, panel, 0, SpringLayout.EAST, getContentPane());
		getContentPane().add(panel);
		panel.setLayout(null);

		JButton btn_save = new JButton(" \u00C4nderungen Speichern ");
		panel.add(btn_save);
		springLayout.putConstraint(SpringLayout.WEST, btn_save, 36, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btn_save, -828, SpringLayout.EAST, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, btn_save, -10, SpringLayout.SOUTH, getContentPane());
		btn_save.setBounds(58, 11, 221, 26);
		btn_save.setIcon(new ImageIcon("media/save.png"));
		Main.defaultButton(btn_save);

		JButton btn_back = new JButton("");
		btn_back.setIcon(new ImageIcon("media/arrowLeft.png"));
		btn_back.setBackground(Main.BACKGROUND);
		btn_back.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 0));
		btn_back.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				save(index);
				Main.setApplicationWindow(new ListAccounts());
			}
		});
		btn_back.setBounds(0, 0, 48, 48);
		panel.add(btn_back);
		
		if (index >= 0) {
			btn_delete = new JButton(" Account l\u00F6schen");
			btn_delete.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int dialogButton = JOptionPane.YES_NO_OPTION;
					int dialogResult = JOptionPane.showConfirmDialog(null,
							"M�chten Sie diesen Account wirklich unwiederruflich l�schen?", "Warnung", dialogButton);
					if (dialogResult == JOptionPane.YES_OPTION) {
						Main.removeAccount(index);
						WriteFile.writeFile(Main.ACCOUT_FILE_PATH + Main.getUserName() + Main.ACCOUT_FILE_TYPE, false);
						Main.setApplicationWindow(new ListAccounts());
					}
				}
			});
			btn_delete.setBounds(898, 11, 186, 26);
			panel.add(btn_delete);
			btn_delete.setIcon(new ImageIcon("media/delete.png"));
			Main.defaultButton(btn_delete);
			

			btn_restore = new JButton("\u00C4nderungen verwerfen");
			btn_restore.setIcon(new ImageIcon("media/clear.png"));
			btn_restore.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setTextBoxes(index);
				}
			});
			Main.defaultButton(btn_restore);
			btn_restore.setBounds(289, 11, 221, 26);
			panel.add(btn_restore);

		}

		btn_save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				save(index);
			}
		});

		JButton btn_openInBrowser = new JButton(" \u00D6ffnen");
		springLayout.putConstraint(SpringLayout.NORTH, btn_openInBrowser, 6, SpringLayout.SOUTH, btn_clipboardPassword);
		springLayout.putConstraint(SpringLayout.WEST, btn_openInBrowser, 6, SpringLayout.EAST, tf_url);
		springLayout.putConstraint(SpringLayout.SOUTH, btn_openInBrowser, 38, SpringLayout.SOUTH,
				btn_clipboardPassword);
		springLayout.putConstraint(SpringLayout.EAST, btn_openInBrowser, -10, SpringLayout.EAST, getContentPane());
		btn_openInBrowser.setForeground(Color.WHITE);
		btn_openInBrowser.setBackground(Main.BUTTON_BACKGROUND);
		btn_openInBrowser.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 0));
		btn_openInBrowser.setIcon(new ImageIcon("media/openInBrowser.png"));
		btn_openInBrowser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Main.openBrowser(tf_url.getText());
			}
		});
		getContentPane().add(btn_openInBrowser);

	}

	private static void setTextBoxes(int i) {

		String[] stringArray = Main.getAccount(index);

		try {
			tf_title.setText(Decrypt.decrypt(stringArray[0], Main.getMasterPassword()));

			tf_username.setText(Decrypt.decrypt(stringArray[1], Main.getMasterPassword()));
			pf_password.setText(Decrypt.decrypt(stringArray[2], Main.getMasterPassword()));
			tf_url.setText(Decrypt.decrypt(stringArray[3], Main.getMasterPassword()));

			tf_attribute1Value.setText(Decrypt.decrypt(stringArray[5], Main.getMasterPassword()));
			tf_attribute1Name.setText(Decrypt.decrypt(stringArray[4], Main.getMasterPassword()));
			tf_attribute2Name.setText(Decrypt.decrypt(stringArray[6], Main.getMasterPassword()));
			tf_attribute2Value.setText(Decrypt.decrypt(stringArray[7], Main.getMasterPassword()));
			tf_attribute3Name.setText(Decrypt.decrypt(stringArray[8], Main.getMasterPassword()));
			tf_attribute3Value.setText(Decrypt.decrypt(stringArray[9], Main.getMasterPassword()));
			tf_attribute4Name.setText(Decrypt.decrypt(stringArray[10], Main.getMasterPassword()));
			tf_attribute4Value.setText(Decrypt.decrypt(stringArray[11], Main.getMasterPassword()));
			tf_attribute5Name.setText(Decrypt.decrypt(stringArray[12], Main.getMasterPassword()));
			tf_attribute5Value.setText(Decrypt.decrypt(stringArray[13], Main.getMasterPassword()));
			tf_attribute6Name.setText(Decrypt.decrypt(stringArray[14], Main.getMasterPassword()));
			tf_attribute6Value.setText(Decrypt.decrypt(stringArray[15], Main.getMasterPassword()));
			tf_attribute7Name.setText(Decrypt.decrypt(stringArray[16], Main.getMasterPassword()));
			tf_attribute7Value.setText(Decrypt.decrypt(stringArray[17], Main.getMasterPassword()));
			tf_attribute8Name.setText(Decrypt.decrypt(stringArray[18], Main.getMasterPassword()));
			tf_attribute8Value.setText(Decrypt.decrypt(stringArray[19], Main.getMasterPassword()));
			tf_attribute9Name.setText(Decrypt.decrypt(stringArray[20], Main.getMasterPassword()));
			tf_attribute9Value.setText(Decrypt.decrypt(stringArray[21], Main.getMasterPassword()));
			tf_attribute10Name.setText(Decrypt.decrypt(stringArray[22], Main.getMasterPassword()));
			tf_attribute10Value.setText(Decrypt.decrypt(stringArray[23], Main.getMasterPassword()));
		} catch (InvalidKeyException | UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException
				| IllegalBlockSizeException | BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Main.setApplicationWindow(new ListAccounts());
		}
	}

	private static void save(int i) {
		if (tf_title.getText().equals("") || tf_username.getText().equals("")
				|| pf_password.getPassword().length == 0) {
			lbl_error.setText("Die mit * als Pflichtfelder gekennzeichneten Felder m�ssen ausgef�llt werden.");
		} else {
			try {
				String[] stringArray = { Encrypt.encrypt(tf_title.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_username.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(new String(pf_password.getPassword()), Main.getMasterPassword()),
						Encrypt.encrypt(tf_url.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute1Name.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute1Value.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute2Name.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute2Value.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute3Name.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute3Value.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute4Name.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute4Value.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute5Name.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute5Value.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute6Name.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute6Value.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute7Name.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute7Value.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute8Name.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute8Value.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute9Name.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute9Value.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute10Name.getText(), Main.getMasterPassword()),
						Encrypt.encrypt(tf_attribute10Value.getText(), Main.getMasterPassword()), "\n" };

				if (index < 0) {
					Main.addAccount(stringArray);
					lbl_error.setText("Der neue Account wurde hinzugef�gt.");
					index = Main.getAccounts().indexOf(stringArray);
				} else {
					Main.setAccount(index, stringArray);
					lbl_error.setText("Die �nderungen wurden �bernommen.");
				}

			} catch (Exception e1) {
				e1.printStackTrace();
				System.out.println("Failed to save account details");
				lbl_error.setText("Die �nderungen konnten nicht gespeichtert werden.");
			}

		}
		WriteFile.writeFile(Main.ACCOUT_FILE_PATH + Main.getUserName() + Main.ACCOUT_FILE_TYPE, false);

	}
}
