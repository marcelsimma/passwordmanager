package at.reinsimma.passwordmanager.gui;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.SystemColor;
import javax.swing.SwingConstants;
import java.awt.Label;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import at.reinsimma.passwordmanager.main.CopyFile;
import at.reinsimma.passwordmanager.main.Main;
import at.reinsimma.passwordmanager.main.ReadFile;
import javax.swing.JTextPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Login extends JFrame {

	private JTextField tf_enterUsername;
	private JPasswordField pf_enterMasterpassword;

	public Login() {
		getContentPane().setBackground(new Color(26, 26, 26));
		this.setResizable(false);
		this.setBounds(100, 100, 1100, 700);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		getContentPane().setLayout(springLayout);

		Label lbl_copyrightInformation = new Label("\u00A9 2018-2019 Reinsimma GmbH");
		springLayout.putConstraint(SpringLayout.NORTH, lbl_copyrightInformation, -32, SpringLayout.SOUTH,
				getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lbl_copyrightInformation, 0, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, lbl_copyrightInformation, 0, SpringLayout.SOUTH,
				getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lbl_copyrightInformation, 0, SpringLayout.EAST, getContentPane());
		lbl_copyrightInformation.setBackground(new Color(26, 26, 26));
		lbl_copyrightInformation.setAlignment(Label.CENTER);
		lbl_copyrightInformation.setForeground(Color.WHITE);
		getContentPane().add(lbl_copyrightInformation);

		JPanel pnl_content = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, pnl_content, 50, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, pnl_content, 370, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, pnl_content, -39, SpringLayout.NORTH, lbl_copyrightInformation);
		springLayout.putConstraint(SpringLayout.EAST, pnl_content, -364, SpringLayout.EAST, getContentPane());
		pnl_content.setBackground(Main.BACKGROUND);
		getContentPane().add(pnl_content);
		pnl_content.setLayout(null);

		JLabel lbl_heading = new JLabel("Anmelden");
		lbl_heading.setBounds(10, 11, 340, 40);
		pnl_content.add(lbl_heading);
		lbl_heading.setForeground(SystemColor.text);
		lbl_heading.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_heading.setFont(new Font("Calibri Light", Font.PLAIN, 32));

		JLabel lbl_username = new JLabel("Benutzername");
		lbl_username.setBounds(10, 62, 167, 15);
		pnl_content.add(lbl_username);
		springLayout.putConstraint(SpringLayout.NORTH, lbl_username, 44, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lbl_username, -208, SpringLayout.EAST, getContentPane());
		lbl_username.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbl_username.setForeground(Color.WHITE);

		tf_enterUsername = new JTextField();
		tf_enterUsername.setBounds(10, 84, 340, 32);
		pnl_content.add(tf_enterUsername);
		springLayout.putConstraint(SpringLayout.NORTH, tf_enterUsername, 119, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, tf_enterUsername, -208, SpringLayout.EAST, getContentPane());
		Main.defaultTextBox(tf_enterUsername);

		JLabel lbl_masterpassword = new JLabel("Masterpasswort");
		lbl_masterpassword.setBounds(10, 140, 85, 15);
		pnl_content.add(lbl_masterpassword);
		springLayout.putConstraint(SpringLayout.NORTH, lbl_masterpassword, 215, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lbl_masterpassword, -154, SpringLayout.EAST, getContentPane());
		lbl_masterpassword.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbl_masterpassword.setForeground(Color.WHITE);
		springLayout.putConstraint(SpringLayout.WEST, lbl_heading, -351, SpringLayout.WEST, lbl_masterpassword);

		pf_enterMasterpassword = new JPasswordField();
		pf_enterMasterpassword.setBounds(10, 163, 340, 32);
		pnl_content.add(pf_enterMasterpassword);
		springLayout.putConstraint(SpringLayout.NORTH, pf_enterMasterpassword, 0, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, pf_enterMasterpassword, 0, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, pf_enterMasterpassword, 55, SpringLayout.WEST, getContentPane());
		Main.defaultPasswordField(pf_enterMasterpassword);

		JButton btn_login = new JButton("Anmelden");
		btn_login.setBounds(10, 229, 340, 32);
		pnl_content.add(btn_login);
		springLayout.putConstraint(SpringLayout.EAST, btn_login, -307, SpringLayout.EAST, getContentPane());
		btn_login.setFont(new Font("Tahoma", Font.PLAIN, 12));
		Main.defaultButton(btn_login);
		springLayout.putConstraint(SpringLayout.EAST, lbl_heading, -95, SpringLayout.WEST, btn_login);

		Label lbl_information = new Label("Noch keinen Account? Erstellen Sie hier einen!");
		lbl_information.setBounds(10, 479, 340, 22);
		pnl_content.add(lbl_information);
		springLayout.putConstraint(SpringLayout.SOUTH, lbl_information, -557, SpringLayout.SOUTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lbl_information, 523, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lbl_information, -334, SpringLayout.EAST, getContentPane());
		lbl_information.setForeground(Color.WHITE);
		lbl_information.setBackground(new Color(26, 26, 26));
		springLayout.putConstraint(SpringLayout.NORTH, lbl_heading, 145, SpringLayout.SOUTH, lbl_information);
		springLayout.putConstraint(SpringLayout.NORTH, btn_login, 178, SpringLayout.SOUTH, lbl_information);

		JButton btn_register = new JButton("Registrieren");
		btn_register.setBounds(10, 507, 340, 32);
		pnl_content.add(btn_register);
		springLayout.putConstraint(SpringLayout.NORTH, btn_register, 381, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btn_register, -208, SpringLayout.EAST, getContentPane());
		Main.defaultButton(btn_register);

		JTextPane tp_error = new JTextPane();
		tp_error.setForeground(Color.RED);
		tp_error.setBackground(Main.BACKGROUND);
		tp_error.setBounds(10, 272, 340, 201);
		pnl_content.add(tp_error);
		tp_error.setEditable(false);
		btn_register.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				Main.setApplicationWindow(new Register());
			}

		});
		btn_login.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				String tmp1 = tf_enterUsername.getText(), tmp2 = new String(pf_enterMasterpassword.getPassword()), tmp = "";

				if (tmp1.equals("")) {
					tmp = "Bitte geben Sie einen Benutzernamen ein. ";
				} if (tmp1.length() < Main.USERNAME_LENGTH) {
					tmp += ("Der Benuterzname muss mindestens " + Main.USERNAME_LENGTH + " Zeichen lang sein. ");
				} if (tmp2.equals("")) {
					tmp += "Bitte geben Sie das passende Masterpasswort ein. ";
				} if (tmp2.length() < Main.MASTERPASSWORD_LENGTH) {
					tmp += ("Das Masterpasswort muss mindestens " + Main.MASTERPASSWORD_LENGTH + " Zeichen lang sein. ");
				} if(tmp.equals("")) {
					
					Main.setUserName(tmp1);
					Main.setMasterPassword(tmp2);
					
					try {
						
						ReadFile.readfile(Main.ACCOUT_FILE_PATH + Main.getUserName() + Main.ACCOUT_FILE_TYPE);

						try {
							CopyFile.cc();
						} catch (IOException e) {
							e.printStackTrace();
							System.out.println("Failed to copy file.");
						}

						Main.setApplicationWindow(new ListAccounts());
						System.out.println("Loading after login successfull. Creating password list.");

					} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
							| IllegalBlockSizeException | IOException | BadPaddingException e) {
						e.printStackTrace();
						tmp = "Fehler beim entschlüsseln. Bitte melden Sie sich erneut an.";
					}

				}
				tp_error.setText(tmp);
			}

		});

	}
}
