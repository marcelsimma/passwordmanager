package at.reinsimma.passwordmanager.unused;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

class OutputReader {
	
	public static String[][] passwordlist = new String[2][2];
	public static void main(String[] args) throws IOException {
		FileReader fr = new FileReader("config/passwortmanagerfile.txt");
		BufferedReader br = new BufferedReader(fr);


		String line = br.readLine();
		String[] substrings = null;
		while (line != null) {

			substrings = line.split(";", 2);
			for (String temp : substrings) {
				System.out.println(temp);
			}

			line = br.readLine();
		}

		convertInto2D(substrings);
		br.close();
	}

	public static void convertInto2D(String[] string) {
		
		for (int i = 0; i < string.length/2; i++) {
			for (int j = 0; j < string.length; j+=2)
			{
			passwordlist[i][0] = string[j];
			passwordlist[i][1] = string[j+1] + "success";
			}
		}	
	}
}