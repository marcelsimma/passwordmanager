//package at.reinsimma.passwordmanager.unused;
//
//import javax.swing.JFrame;
//import java.awt.Color;
//import javax.swing.JLabel;
//import java.awt.Font;
//import javax.swing.JButton;
//import java.awt.event.ActionListener;
//import java.io.IOException;
//import java.awt.event.ActionEvent;
//import javax.swing.JTextField;
//import javax.swing.SwingConstants;
//
//import at.reinsimma.passwordmanager.main.Main;
//import at.reinsimma.passwordmanager.main.ReadFile;
//import at.reinsimma.passwordmanager.main.WriteFile;
//
//import javax.swing.JPasswordField;
//import javax.swing.JPanel;
//import javax.swing.SpringLayout;
//
//public class AddNewPassword extends JFrame {
//
//	
//	private JTextField tf_application;
//	private JPasswordField pf_password1;
//	private JPasswordField pf_password2;
//	private JTextField tf_username;
//	private JTextField tf_attribute1;
//	private JTextField tf_attribute2;
//	private JTextField tf_attribute3;
//	private JTextField tf_attribute4;
//	private JTextField tf_attribute5;
//	private JTextField tf_attribute6;
//	private JTextField tf_attribute7;
//	private JLabel lblBenutzername;
//	private JLabel lblEmail;
//	private JLabel lblWiederherstellungsemailadresse;
//	private JLabel lblAttribut;
//	private JLabel lblAttribut_1;
//	private JLabel lblAttribut_2;
//	private JLabel lblAttribut_3;
//	private JLabel lblAttribut_4;
//	private JLabel lbl_information;
//
//	public AddNewPassword() {
//		getContentPane().setBackground(Color.LIGHT_GRAY);
//		initialize();
//	}
//	
//	public void initialize() {
//		
//		this.setResizable(false);
//		this.setBackground(new Color(26,26,26));
//		this.setBounds(100, 100, 1100, 700);
//		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		this.getContentPane().setLayout(null);
//
//		JLabel lbl_heading = new JLabel("Neues Passwort hinzuf\u00FCgen");
//		lbl_heading.setFont(new Font("Calibri Light", Font.PLAIN, 32));
//		lbl_heading.setBounds(12, 25, 970, 40);
//		this.getContentPane().add(lbl_heading);
//				
//				JPanel panel = new JPanel();
//				panel.setBounds(12, 67, 592, 484);
//				getContentPane().add(panel);
//						SpringLayout sl_panel = new SpringLayout();
//						panel.setLayout(sl_panel);
//				
//						tf_application = new JTextField();
//						sl_panel.putConstraint(SpringLayout.NORTH, tf_application, 10, SpringLayout.NORTH, panel);
//						sl_panel.putConstraint(SpringLayout.WEST, tf_application, 234, SpringLayout.WEST, panel);
//						sl_panel.putConstraint(SpringLayout.SOUTH, tf_application, 42, SpringLayout.NORTH, panel);
//						sl_panel.putConstraint(SpringLayout.EAST, tf_application, -10, SpringLayout.EAST, panel);
//						panel.add(tf_application);
//						tf_application.setColumns(10);
//						
//								JLabel lblBitteGebenSie = new JLabel("Verwendungsort: *");
//								sl_panel.putConstraint(SpringLayout.EAST, lblBitteGebenSie, -6, SpringLayout.WEST, tf_application);
//								sl_panel.putConstraint(SpringLayout.NORTH, lblBitteGebenSie, 18, SpringLayout.NORTH, panel);
//								sl_panel.putConstraint(SpringLayout.WEST, lblBitteGebenSie, 10, SpringLayout.WEST, panel);
//								panel.add(lblBitteGebenSie);
//								lblBitteGebenSie.setFont(new Font("Tahoma", Font.PLAIN, 12));
//								lblBitteGebenSie.setHorizontalAlignment(SwingConstants.LEFT);
//								
//										JLabel lblPasswort = new JLabel("Passwort: *");
//										sl_panel.putConstraint(SpringLayout.WEST, lblPasswort, 10, SpringLayout.WEST, panel);
//										panel.add(lblPasswort);
//										lblPasswort.setFont(new Font("Tahoma", Font.PLAIN, 12));
//										lblPasswort.setHorizontalAlignment(SwingConstants.LEFT);
//										
//												JLabel lblPasswortWiederholen = new JLabel("Passwort wiederholen: *");
//												sl_panel.putConstraint(SpringLayout.NORTH, lblPasswortWiederholen, 23, SpringLayout.SOUTH, lblPasswort);
//												sl_panel.putConstraint(SpringLayout.WEST, lblPasswortWiederholen, 10, SpringLayout.WEST, panel);
//												panel.add(lblPasswortWiederholen);
//												lblPasswortWiederholen.setFont(new Font("Tahoma", Font.PLAIN, 12));
//												lblPasswortWiederholen.setHorizontalAlignment(SwingConstants.LEFT);
//												
//														pf_password1 = new JPasswordField();
//														sl_panel.putConstraint(SpringLayout.EAST, lblPasswort, -6, SpringLayout.WEST, pf_password1);
//														sl_panel.putConstraint(SpringLayout.NORTH, pf_password1, 44, SpringLayout.SOUTH, tf_application);
//														sl_panel.putConstraint(SpringLayout.WEST, pf_password1, 0, SpringLayout.WEST, tf_application);
//														sl_panel.putConstraint(SpringLayout.EAST, pf_password1, -10, SpringLayout.EAST, panel);
//														panel.add(pf_password1);
//														
//																pf_password2 = new JPasswordField();
//																sl_panel.putConstraint(SpringLayout.EAST, lblPasswortWiederholen, -6, SpringLayout.WEST, pf_password2);
//																sl_panel.putConstraint(SpringLayout.NORTH, pf_password2, 124, SpringLayout.NORTH, panel);
//																sl_panel.putConstraint(SpringLayout.WEST, pf_password2, 0, SpringLayout.WEST, tf_application);
//																sl_panel.putConstraint(SpringLayout.EAST, pf_password2, -10, SpringLayout.EAST, panel);
//																panel.add(pf_password2);
//																
//																tf_username = new JTextField();
//																sl_panel.putConstraint(SpringLayout.NORTH, tf_username, 6, SpringLayout.SOUTH, tf_application);
//																sl_panel.putConstraint(SpringLayout.WEST, tf_username, 0, SpringLayout.WEST, tf_application);
//																sl_panel.putConstraint(SpringLayout.SOUTH, tf_username, 38, SpringLayout.SOUTH, tf_application);
//																sl_panel.putConstraint(SpringLayout.EAST, tf_username, 0, SpringLayout.EAST, tf_application);
//																tf_username.setColumns(10);
//																panel.add(tf_username);
//																
//																tf_attribute1 = new JTextField();
//																sl_panel.putConstraint(SpringLayout.SOUTH, pf_password1, -44, SpringLayout.NORTH, tf_attribute1);
//																sl_panel.putConstraint(SpringLayout.NORTH, tf_attribute1, 162, SpringLayout.NORTH, panel);
//																sl_panel.putConstraint(SpringLayout.SOUTH, pf_password2, -6, SpringLayout.NORTH, tf_attribute1);
//																sl_panel.putConstraint(SpringLayout.WEST, tf_attribute1, 0, SpringLayout.WEST, tf_application);
//																sl_panel.putConstraint(SpringLayout.EAST, tf_attribute1, 0, SpringLayout.EAST, tf_application);
//																tf_attribute1.setColumns(10);
//																panel.add(tf_attribute1);
//																
//																tf_attribute2 = new JTextField();
//																sl_panel.putConstraint(SpringLayout.SOUTH, tf_attribute1, -6, SpringLayout.NORTH, tf_attribute2);
//																sl_panel.putConstraint(SpringLayout.NORTH, tf_attribute2, 200, SpringLayout.NORTH, panel);
//																sl_panel.putConstraint(SpringLayout.WEST, tf_attribute2, 0, SpringLayout.WEST, tf_application);
//																sl_panel.putConstraint(SpringLayout.EAST, tf_attribute2, 0, SpringLayout.EAST, tf_application);
//																tf_attribute2.setColumns(10);
//																panel.add(tf_attribute2);
//																
//																tf_attribute3 = new JTextField();
//																sl_panel.putConstraint(SpringLayout.SOUTH, tf_attribute3, -214, SpringLayout.SOUTH, panel);
//																sl_panel.putConstraint(SpringLayout.SOUTH, tf_attribute2, -6, SpringLayout.NORTH, tf_attribute3);
//																sl_panel.putConstraint(SpringLayout.NORTH, tf_attribute3, 238, SpringLayout.NORTH, panel);
//																sl_panel.putConstraint(SpringLayout.WEST, tf_attribute3, 0, SpringLayout.WEST, tf_application);
//																sl_panel.putConstraint(SpringLayout.EAST, tf_attribute3, 0, SpringLayout.EAST, tf_application);
//																tf_attribute3.setColumns(10);
//																panel.add(tf_attribute3);
//																
//																tf_attribute4 = new JTextField();
//																sl_panel.putConstraint(SpringLayout.NORTH, tf_attribute4, 6, SpringLayout.SOUTH, tf_attribute3);
//																sl_panel.putConstraint(SpringLayout.WEST, tf_attribute4, 0, SpringLayout.WEST, tf_application);
//																sl_panel.putConstraint(SpringLayout.SOUTH, tf_attribute4, 38, SpringLayout.SOUTH, tf_attribute3);
//																sl_panel.putConstraint(SpringLayout.EAST, tf_attribute4, 0, SpringLayout.EAST, tf_application);
//																tf_attribute4.setColumns(10);
//																panel.add(tf_attribute4);
//																
//																tf_attribute5 = new JTextField();
//																sl_panel.putConstraint(SpringLayout.NORTH, tf_attribute5, 6, SpringLayout.SOUTH, tf_attribute4);
//																sl_panel.putConstraint(SpringLayout.WEST, tf_attribute5, -348, SpringLayout.EAST, tf_application);
//																sl_panel.putConstraint(SpringLayout.SOUTH, tf_attribute5, 38, SpringLayout.SOUTH, tf_attribute4);
//																sl_panel.putConstraint(SpringLayout.EAST, tf_attribute5, 0, SpringLayout.EAST, tf_application);
//																tf_attribute5.setColumns(10);
//																panel.add(tf_attribute5);
//																
//																tf_attribute6 = new JTextField();
//																sl_panel.putConstraint(SpringLayout.NORTH, tf_attribute6, 6, SpringLayout.SOUTH, tf_attribute5);
//																sl_panel.putConstraint(SpringLayout.WEST, tf_attribute6, 0, SpringLayout.WEST, tf_application);
//																sl_panel.putConstraint(SpringLayout.SOUTH, tf_attribute6, -100, SpringLayout.SOUTH, panel);
//																sl_panel.putConstraint(SpringLayout.EAST, tf_attribute6, 0, SpringLayout.EAST, tf_application);
//																tf_attribute6.setColumns(10);
//																panel.add(tf_attribute6);
//																
//																tf_attribute7 = new JTextField();
//																sl_panel.putConstraint(SpringLayout.NORTH, tf_attribute7, 390, SpringLayout.NORTH, panel);
//																sl_panel.putConstraint(SpringLayout.WEST, tf_attribute7, -348, SpringLayout.EAST, tf_application);
//																sl_panel.putConstraint(SpringLayout.SOUTH, tf_attribute7, 38, SpringLayout.SOUTH, tf_attribute6);
//																sl_panel.putConstraint(SpringLayout.EAST, tf_attribute7, 0, SpringLayout.EAST, tf_application);
//																tf_attribute7.setColumns(10);
//																panel.add(tf_attribute7);
//																
//																		JButton btn_addPassword = new JButton("Passwort hinzuf\u00FCgen");
//																		sl_panel.putConstraint(SpringLayout.NORTH, btn_addPassword, 6, SpringLayout.SOUTH, tf_attribute7);
//																		sl_panel.putConstraint(SpringLayout.WEST, btn_addPassword, 0, SpringLayout.WEST, tf_application);
//																		sl_panel.putConstraint(SpringLayout.SOUTH, btn_addPassword, -24, SpringLayout.SOUTH, panel);
//																		sl_panel.putConstraint(SpringLayout.EAST, btn_addPassword, -43, SpringLayout.EAST, panel);
//																		panel.add(btn_addPassword);
//																		
//																		lblBenutzername = new JLabel("Benutzername: *");
//																		sl_panel.putConstraint(SpringLayout.NORTH, lblBenutzername, 23, SpringLayout.SOUTH, lblBitteGebenSie);
//																		sl_panel.putConstraint(SpringLayout.WEST, lblBenutzername, 10, SpringLayout.WEST, panel);
//																		sl_panel.putConstraint(SpringLayout.EAST, lblBenutzername, -6, SpringLayout.WEST, tf_username);
//																		sl_panel.putConstraint(SpringLayout.NORTH, lblPasswort, 23, SpringLayout.SOUTH, lblBenutzername);
//																		lblBenutzername.setHorizontalAlignment(SwingConstants.LEFT);
//																		lblBenutzername.setFont(new Font("Tahoma", Font.PLAIN, 12));
//																		panel.add(lblBenutzername);
//																		
//																		lblEmail = new JLabel("Attribut6:");
//																		sl_panel.putConstraint(SpringLayout.NORTH, lblEmail, 8, SpringLayout.NORTH, tf_attribute6);
//																		sl_panel.putConstraint(SpringLayout.WEST, lblEmail, 10, SpringLayout.WEST, panel);
//																		sl_panel.putConstraint(SpringLayout.EAST, lblEmail, 0, SpringLayout.EAST, lblBitteGebenSie);
//																		lblEmail.setHorizontalAlignment(SwingConstants.LEFT);
//																		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 12));
//																		panel.add(lblEmail);
//																		
//																		lblWiederherstellungsemailadresse = new JLabel("Attribut7:");
//																		sl_panel.putConstraint(SpringLayout.NORTH, lblWiederherstellungsemailadresse, 8, SpringLayout.NORTH, tf_attribute7);
//																		sl_panel.putConstraint(SpringLayout.WEST, lblWiederherstellungsemailadresse, 10, SpringLayout.WEST, panel);
//																		sl_panel.putConstraint(SpringLayout.SOUTH, lblWiederherstellungsemailadresse, 23, SpringLayout.NORTH, tf_attribute7);
//																		sl_panel.putConstraint(SpringLayout.EAST, lblWiederherstellungsemailadresse, -6, SpringLayout.WEST, tf_attribute7);
//																		lblWiederherstellungsemailadresse.setHorizontalAlignment(SwingConstants.LEFT);
//																		lblWiederherstellungsemailadresse.setFont(new Font("Tahoma", Font.PLAIN, 12));
//																		panel.add(lblWiederherstellungsemailadresse);
//																		
//																		lblAttribut = new JLabel("Attribut1:");
//																		sl_panel.putConstraint(SpringLayout.NORTH, lblAttribut, 8, SpringLayout.NORTH, tf_attribute1);
//																		sl_panel.putConstraint(SpringLayout.WEST, lblAttribut, 10, SpringLayout.WEST, panel);
//																		sl_panel.putConstraint(SpringLayout.EAST, lblAttribut, 0, SpringLayout.EAST, lblBitteGebenSie);
//																		lblAttribut.setHorizontalAlignment(SwingConstants.LEFT);
//																		lblAttribut.setFont(new Font("Tahoma", Font.PLAIN, 12));
//																		panel.add(lblAttribut);
//																		
//																		lblAttribut_1 = new JLabel("Attribut2:");
//																		sl_panel.putConstraint(SpringLayout.NORTH, lblAttribut_1, 8, SpringLayout.NORTH, tf_attribute2);
//																		sl_panel.putConstraint(SpringLayout.WEST, lblAttribut_1, 0, SpringLayout.WEST, lblBitteGebenSie);
//																		sl_panel.putConstraint(SpringLayout.EAST, lblAttribut_1, 0, SpringLayout.EAST, lblBitteGebenSie);
//																		lblAttribut_1.setHorizontalAlignment(SwingConstants.LEFT);
//																		lblAttribut_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
//																		panel.add(lblAttribut_1);
//																		
//																		lblAttribut_2 = new JLabel("Attribut3:");
//																		sl_panel.putConstraint(SpringLayout.NORTH, lblAttribut_2, 8, SpringLayout.NORTH, tf_attribute3);
//																		sl_panel.putConstraint(SpringLayout.WEST, lblAttribut_2, 0, SpringLayout.WEST, lblBitteGebenSie);
//																		sl_panel.putConstraint(SpringLayout.EAST, lblAttribut_2, 0, SpringLayout.EAST, lblBitteGebenSie);
//																		lblAttribut_2.setHorizontalAlignment(SwingConstants.LEFT);
//																		lblAttribut_2.setFont(new Font("Tahoma", Font.PLAIN, 12));
//																		panel.add(lblAttribut_2);
//																		
//																		lblAttribut_3 = new JLabel("Attribut4:");
//																		sl_panel.putConstraint(SpringLayout.NORTH, lblAttribut_3, 8, SpringLayout.NORTH, tf_attribute4);
//																		sl_panel.putConstraint(SpringLayout.WEST, lblAttribut_3, 10, SpringLayout.WEST, panel);
//																		sl_panel.putConstraint(SpringLayout.EAST, lblAttribut_3, 0, SpringLayout.EAST, lblBitteGebenSie);
//																		lblAttribut_3.setHorizontalAlignment(SwingConstants.LEFT);
//																		lblAttribut_3.setFont(new Font("Tahoma", Font.PLAIN, 12));
//																		panel.add(lblAttribut_3);
//																		
//																		lblAttribut_4 = new JLabel("Attribut5:");
//																		sl_panel.putConstraint(SpringLayout.NORTH, lblAttribut_4, 8, SpringLayout.NORTH, tf_attribute5);
//																		sl_panel.putConstraint(SpringLayout.WEST, lblAttribut_4, 0, SpringLayout.WEST, lblBitteGebenSie);
//																		sl_panel.putConstraint(SpringLayout.EAST, lblAttribut_4, 0, SpringLayout.EAST, lblBitteGebenSie);
//																		lblAttribut_4.setHorizontalAlignment(SwingConstants.LEFT);
//																		lblAttribut_4.setFont(new Font("Tahoma", Font.PLAIN, 12));
//																		panel.add(lblAttribut_4);
//																		
//																		lbl_information = new JLabel("* = Pflichtfeld");
//																		sl_panel.putConstraint(SpringLayout.NORTH, lbl_information, 8, SpringLayout.NORTH, btn_addPassword);
//																		sl_panel.putConstraint(SpringLayout.WEST, lbl_information, 0, SpringLayout.WEST, lblBitteGebenSie);
//																		sl_panel.putConstraint(SpringLayout.EAST, lbl_information, 0, SpringLayout.EAST, lblBitteGebenSie);
//																		lbl_information.setHorizontalAlignment(SwingConstants.LEFT);
//																		lbl_information.setFont(new Font("Tahoma", Font.PLAIN, 12));
//																		panel.add(lbl_information);
//				btn_addPassword.addActionListener(new ActionListener() {
//					@Override
//					public void actionPerformed(ActionEvent arg0) {
////						try {
////							//addNewPassword();
////						} catch (IOException e) {
////							// TODO Auto-generated catch block
////							e.printStackTrace();
////						}
////					}
//				});
//
//	}
//
////	public void addNewPassword() throws IOException {
////		if (new String(pf_password1.getPassword())
////				.equals(new String(pf_password2.getPassword()))) {
////			
////			ReadFile.readfile("config/passwords.txt");
////
////			Main.content.add(new String[] { tf_application.getText(),
////					new String(pf_password1.getPassword())});
////
////			WriteFile.writeFile("passwords.txt", false);
////			System.out.println("�alksdf");
////			
////		}
////
////	}
//}
