package at.reinsimma.passwordmanager.unused;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JPasswordField;

public class ListAllPasswords {

	private JFrame frame;
	private JTextField textFieldPasswordName;
	private JPasswordField passwordFieldAddPassword1;
	private JPasswordField passwordFieldAddPassword2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListAllPasswords window = new ListAllPasswords();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ListAllPasswords() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBackground(Color.WHITE);
		frame.setBounds(100, 100, 1000, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPasswordmanager = new JLabel("Neues Passwort hinzuf\u00FCgen");
		lblPasswordmanager.setFont(new Font("Calibri Light", Font.PLAIN, 32));
		lblPasswordmanager.setBounds(12, 13, 970, 40);
		frame.getContentPane().add(lblPasswordmanager);
		
		JButton btnAddPassword = new JButton("Passwort hinzuf\u00FCgen");
		btnAddPassword.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				addNewPassword();
			}
		});
		btnAddPassword.setBounds(31, 230, 162, 33);
		frame.getContentPane().add(btnAddPassword);
		
		JLabel lblBitteGebenSie = new JLabel("Name");
		lblBitteGebenSie.setHorizontalAlignment(SwingConstants.RIGHT);
		lblBitteGebenSie.setBounds(80, 99, 235, 16);
		frame.getContentPane().add(lblBitteGebenSie);
		
		textFieldPasswordName = new JTextField();
		textFieldPasswordName.setBounds(329, 96, 653, 22);
		frame.getContentPane().add(textFieldPasswordName);
		textFieldPasswordName.setColumns(10);
		
		JLabel lblPasswort = new JLabel("Passwort");
		lblPasswort.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPasswort.setBounds(80, 128, 235, 16);
		frame.getContentPane().add(lblPasswort);
		
		JLabel lblPasswortWiederholen = new JLabel("Passwort wiederholen");
		lblPasswortWiederholen.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPasswortWiederholen.setBounds(80, 158, 235, 16);
		frame.getContentPane().add(lblPasswortWiederholen);
		
		passwordFieldAddPassword1 = new JPasswordField();
		passwordFieldAddPassword1.setBounds(329, 125, 653, 22);
		frame.getContentPane().add(passwordFieldAddPassword1);
		
		passwordFieldAddPassword2 = new JPasswordField();
		passwordFieldAddPassword2.setBounds(329, 155, 653, 22);
		frame.getContentPane().add(passwordFieldAddPassword2);
		
	}
	
	public void addNewPassword() {
		if (new String(passwordFieldAddPassword1.getPassword()).equals(new String(passwordFieldAddPassword2.getPassword()))) {
			System.out.println("HAllo");
			String newPassword[][] = new String[1][2];
			newPassword[0][0] = textFieldPasswordName.getText();
			newPassword[0][1] = new String(passwordFieldAddPassword1.getPassword());
		}
		System.out.println("�alksdf");
	}
}
