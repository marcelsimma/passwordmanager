package at.reinsimma.passwordmanager.unused;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FileOutput {
	String[][] passwords = new String[2][2];
	
	public FileOutput(String[][] passwords) {
		super();
		this.passwords = passwords;
	}
	public static void main(String[] args) throws IOException {
		//FileOutput.fillFile(ReadFile.readfile());
	}

	

	public static void fillFile(ArrayList<String[]> string) {
		// TODO Auto-generated method stub
		File datei = new File("config/passwortmanagerfile.txt");
		
		
		String[][] strings = new String[2][2];
	strings[0][0] = "Klaus";
	strings[0][1] = "Klausapasswort";
	strings[1][0] = "Willi";
	strings[1][1] = "Willipassworts";
	
	ArrayList a1 = new ArrayList();
	for (int i= 0; i < strings.length; i++) {
		a1.add(strings[i][0] + ";" + strings[i][1]);
	}	

		PrintWriter printWriter = null;
		FileWriter writer;
		
		try {
			
			writer = new FileWriter(datei, false);
			
			for (String[] temp : string) {
				writer.write(temp[0] + ";" +  temp[1]);
				writer.write(System.getProperty("line.separator"));
			}
			
			
			writer.flush();
			writer.close();
			printWriter = new PrintWriter(new FileWriter(datei, true));
			Iterator iter = a1.iterator();

			while (iter.hasNext()) {
				Object o = iter.next();
				printWriter.println(o);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (printWriter != null)
				printWriter.close();
		}
	}
		
//		strings = new String[2][2];
////		strings[0][0] = "Klaus";
////		strings[0][1] = "Klausapasswort";
////		strings[1][0] = "Willi";
////		strings[1][1] = "Willipassworts";
//
//		ArrayList a1 = new ArrayList();
//		for (int i= 1; i < strings.length; i++) {
//			a1.add(strings[i][0] + ";" + strings[i][1]);
//		}
//
//		listToFile(a1, datei, strings[0][0] + ";" + strings[0][1]);
	

	private static void listToFile(List list, File datei, String string) {
		PrintWriter printWriter = null;
		FileWriter writer;
		try {
			writer = new FileWriter(datei);
			writer.write(string);
			writer.write(System.getProperty("line.separator"));
			writer.flush();
			writer.close();
			printWriter = new PrintWriter(new FileWriter(datei, true));
			Iterator iter = list.iterator();

			while (iter.hasNext()) {
				Object o = iter.next();
				printWriter.println(o);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (printWriter != null)
				printWriter.close();
		}
	}
}
