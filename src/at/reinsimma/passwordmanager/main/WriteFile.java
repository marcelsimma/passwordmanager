package at.reinsimma.passwordmanager.main;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import at.reinsimma.passwordmanager.encryption.Encrypt;

public class WriteFile {

	public static void writeFile(String fileName, boolean deleteOldContent) {
		PrintWriter pWriter = null;
		try {
			pWriter = new PrintWriter(new BufferedWriter(new FileWriter(fileName, deleteOldContent)));

			for (String[] c : Main.getAccounts()) {

				String tmp = "";
				int arrayLength = c.length;

				for (int i = 0; i < arrayLength; i++) {
					if (i + 1 == arrayLength) {
						tmp += c[i];
					} else {
						tmp += (c[i] + "';'");
					}

				}
				try {
				pWriter.println(Encrypt.encrypt(tmp, Main.getMasterPassword()));
				} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Failed to write into File.");
				}
			}

		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			if (pWriter != null) {
				pWriter.flush();
				pWriter.close();
			}
		}
	}
	
	public static void writeLine(String file, String content, boolean deleteOldContent) {
		PrintWriter pWriter = null;
		try {
			pWriter = new PrintWriter(new BufferedWriter(new FileWriter(file, deleteOldContent)));

			try {
				pWriter.println(Encrypt.encrypt(content, Main.getMasterPassword()));
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Failed to write into File.");
			}

		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			if (pWriter != null) {
				pWriter.flush();
				pWriter.close();
			}
		}
	}
}
//