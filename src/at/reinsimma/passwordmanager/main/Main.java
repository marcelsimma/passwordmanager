package at.reinsimma.passwordmanager.main;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import at.reinsimma.passwordmanager.gui.Login;

public class Main {
	
	private static JFrame applicationWindow;
	private static ArrayList<String[]> accounts = new ArrayList<>();
	private static String userName, masterPassword;
	
	public static final Color BACKGROUND = new Color(26, 26, 26), BUTTON_BACKGROUND = new Color(0x4c4c4c),
			TRANSPARENT = new Color(0, 0, 0, 0);
	public static final String ACCOUT_FILE_PATH = "config/", ACCOUT_FILE_PATH_OLD = "config/old/",
			ACCOUT_FILE_TYPE = ".txt";
	public static final int MASTERPASSWORD_LENGTH = 12, USERNAME_LENGTH = 6;

	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					Main.applicationWindow = new JFrame();
					Main.applicationWindow.setSize(1100, 700);
					Main.applicationWindow.setResizable(false);
					Main.applicationWindow.setTitle("Passwortmanager");
					Main.setApplicationWindow(new Login());

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static ArrayList<String[]> getAccounts() {
		return accounts;
	}
	
	public static void addAccount(String[] stringArray) {
		Main.accounts.add(stringArray);
	}
	
	public static void removeAccount(int index) {
		Main.accounts.remove(index);
	}
	
	public static String[] getAccount(int index) {
		return Main.accounts.get(index);
	}
	
	public static void setAccount(int index, String[] stringArray) {
		Main.accounts.set(index, stringArray);
	}

	public static String getUserName() {
		return userName;
	}

	public static void setApplicationWindow(JFrame frame) {
		Main.applicationWindow.setContentPane(frame.getContentPane());
		Main.applicationWindow.setVisible(true);
	}

	public static String getMasterPassword() {
		return masterPassword;
	}

	public static void setUserName(String userName) {
		Main.userName = userName;
	}

	public static void setMasterPassword(String masterPassword) {
		Main.masterPassword = masterPassword;
	}

	public static void openBrowser(String url) {
		if (Desktop.isDesktopSupported()) {
			// Windows
			try {
				Desktop.getDesktop().browse(new URI(url));
			} catch (IOException e) {
				System.out.println("Failed to open URL in browser.");
				e.printStackTrace();
			} catch (URISyntaxException e) {
				System.out.println("Failed to open URL in browser.");
				e.printStackTrace();
			}
		} else {
			// Ubuntu
			Runtime runtime = Runtime.getRuntime();
			try {
				runtime.exec("/usr/bin/firefox -new-window " + url);
			} catch (IOException e) {
				System.out.println("Failed to open URL in browser.");
				e.printStackTrace();
			}
		}
	}

	public static void copyToClipboard(String string) {
		StringSelection selection = new StringSelection(string);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(selection, selection);
	}

	public static String generatePassword(int length) {
		String symbol = "-/.^&*_!@%=+>)";
		String cap_letter = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String small_letter = "abcdefghijklmnopqrstuvwxyz";
		String numbers = "0123456789";

		String finalString = cap_letter + small_letter + numbers + symbol;

		Random random = new Random();

		char[] password = new char[length];

		for (int i = 0; i < length; i++) {
			password[i] = finalString.charAt(random.nextInt(finalString.length()));

		}
		return new String(password);
	}
	
	public static void defaultButton(JButton button) {
		button.setForeground(Color.WHITE);
		button.setFont(new Font("Tahoma", Font.PLAIN, 12));
		button.setBackground(new Color(0x4c4c4c));
		button.setBorder(BorderFactory.createLineBorder(Color.WHITE, 0));
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				button.setBorder(BorderFactory.createLineBorder(Color.WHITE, 0));
				button.setBackground(new Color(0x3c3c3c));
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				button.setBackground(new Color(0x4c4c4c));
				button.setBorder(BorderFactory.createLineBorder(Color.WHITE, 0));
			}
		});
	}
	
	public static void defaultTextBox(JTextField object) {
		object.setForeground(SystemColor.text);
		object.setCaretColor(Color.WHITE);
		object.setColumns(10);
		object.setBackground(Main.BACKGROUND);
		object.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
	}

	public static void defaultPasswordField(JTextField object) {
		object.setForeground(SystemColor.text);
		object.setCaretColor(Color.WHITE);
		object.setBackground(Main.BACKGROUND);
		object.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1));
	}
}
