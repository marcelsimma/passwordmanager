package at.reinsimma.passwordmanager.main;

import java.io.File;
import java.io.IOException;

import at.reinsimma.passwordmanager.encryption.Encrypt;

//
public class CreateFile {

	private boolean checkCreatedFile(File file) {
		if (file != null) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				System.err.println("Error creating " + file.toString());
			}
			if (file.isFile() && file.canWrite() && file.canRead())
				return true;
		}
		return false;
	}

	public static String createFile(String fileName) {
		String dat = (Main.ACCOUT_FILE_PATH + fileName + Main.ACCOUT_FILE_TYPE), result = "";
		
		// is file already existing?
		if (new File(dat).exists()) {
			String error = "The file '" + dat + "' already exists. No new file created.";
			System.out.println(error);
			result = "Es existiert bereits eine Datei mit dem Namen '" + fileName + "'. Verwenden Sie bitte einen anderen Benutzernamen oder melden Sie sich an.";
		
		// create new file if not existing
		} else {
			CreateFile da = new CreateFile();
			if (da.checkCreatedFile(new File(dat))) {
				System.out.println(dat + " erzeugt");
				result = "Die Registrierung mit dem Benutzernamen '" + fileName + "' war erfolgreich. Bitte melden Sie sich an, um die Registrierung abzuschlie�en." ;
			}
		}
		return result;
	}

}
