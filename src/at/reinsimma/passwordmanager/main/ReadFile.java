package at.reinsimma.passwordmanager.main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import at.reinsimma.passwordmanager.encryption.Decrypt;

public class ReadFile
//Source: http://www.javaschubla.de/2007/javaerst0250.html
{
	public static void readfile(String filePath) throws InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException {
		FileReader fr = new FileReader(filePath);
		BufferedReader br = new BufferedReader(fr);

		String zeile = "";

		while ((zeile = br.readLine()) != null) {

			zeile = Decrypt.decrypt(zeile, Main.getMasterPassword());

			String[] temp = zeile.split("';'");

			Main.addAccount(temp);
		}

		br.close();
	}
}