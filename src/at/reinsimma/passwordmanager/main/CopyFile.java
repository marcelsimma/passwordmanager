package at.reinsimma.passwordmanager.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;

public class CopyFile {
    
    public static void cc() throws IOException {
	
        File inF = new File(Main.ACCOUT_FILE_PATH + Main.getUserName() + Main.ACCOUT_FILE_TYPE);
        File outF = new File(Main.ACCOUT_FILE_PATH_OLD + Main.getUserName() + "_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new java.util.Date()) + Main.ACCOUT_FILE_TYPE);
        copyFile(inF, outF);
    }
    
    public static void copyFile(File in, File out) throws IOException {
        FileChannel inChannel = null;
        FileChannel outChannel = null;
        try {
            inChannel = new FileInputStream(in).getChannel();
            outChannel = new FileOutputStream(out).getChannel();
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } catch (IOException e) {
            System.out.println("failed to copy");

        	throw e;
        } finally {
            try {
                if (inChannel != null)
                    inChannel.close();
                if (outChannel != null)
                    outChannel.close();
            } catch (IOException e) {}
        }
    }
} 