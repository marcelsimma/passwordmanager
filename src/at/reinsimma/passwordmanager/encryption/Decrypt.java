package at.reinsimma.passwordmanager.encryption;

import java.awt.EventQueue;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import java.util.Base64;

public class Decrypt {

	/*
	 * Testmain for Encrypt and Decrypt, if working will sysout:
	 * IwNIgxZDSfzg4EIrKKTKSYGGo971oQZzIqSkteDhDrE= Das ist der Text
	 */

// Testmain:	
	public static void main(String[] args) throws Exception {
		Decrypt.decrypt(Encrypt.encrypt("Jackob isch an klina gschrop", "kruzefixsackzeminnamolekruzet�rkanamol"), "kruzefixsackzeminnamolekruzet�rkanamol");
		
		
	}

	// Throws Declarations:
	/**
	 * @param args
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws InvalidKeyException
	 * @throws Exception
	 */

	public static String decrypt(String decryptme, String masterpassword)
			throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException, InvalidKeyException {

		// Das Passwort bzw der Schluesseltext: masterpassword
		// byte-Array erzeugen
		byte[] key = (masterpassword).getBytes("UTF-8");
		// aus dem Array einen Hash-Wert erzeugen mit MD5 oder SHA
		MessageDigest sha = MessageDigest.getInstance("SHA-256");
		key = sha.digest(key);
		// nur die ersten 128 bit nutzen
		key = Arrays.copyOf(key, 16);
		// der fertige Schluessel
		SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");

		// BASE64 String zu Byte-Array konvertieren
		Base64.Decoder myDecoder2 = Base64.getDecoder();
		byte[] crypted2 = myDecoder2.decode(decryptme);

		// Entschluesseln
		Cipher cipher2 = Cipher.getInstance("AES");
		cipher2.init(Cipher.DECRYPT_MODE, secretKeySpec);
		byte[] cipherData2 = cipher2.doFinal(crypted2);
		String decrypted = new String(cipherData2);

		// Klartext
		return decrypted;
	}
}
