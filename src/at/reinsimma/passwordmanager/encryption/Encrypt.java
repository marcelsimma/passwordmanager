package at.reinsimma.passwordmanager.encryption;


import java.security.MessageDigest;
import java.util.Arrays;
 
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
 
import java.util.Base64;
 
public class Encrypt {

	/**
	 * @param args
	 * @throws Exception
	 */

	public static String encrypt(String encryptme, String masterpassword) throws Exception {

		// Das Passwort bzw der Schluesseltext: masterpassword
		// byte-Array erzeugen
		byte[] key = (masterpassword).getBytes("UTF-8");
		// aus dem Array einen Hash-Wert erzeugen mit MD5 oder SHA
		MessageDigest sha = MessageDigest.getInstance("SHA-256");
		key = sha.digest(key);
		// nur die ersten 128 bit nutzen
		key = Arrays.copyOf(key, 16);
		// der fertige Schluessel
		SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");

		// der zu verschl. Text: encryptme
		// Verschluesseln
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
		byte[] encrypted = cipher.doFinal(encryptme.getBytes());

		// bytes zu Base64-String konvertieren (dient der Lesbarkeit)
		Base64.Encoder myEncoder = Base64.getEncoder();
		String secret = myEncoder.encodeToString(encrypted);

		// Ergebnis
		return (secret);
   }
 
}